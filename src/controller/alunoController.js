module.exports = class alunoController {

    static async postAluno(req,res) {
        const { nome, idade, profissao, cursoMatriculado } = req.body;
        console.log ("Segue todas as informações do aluno ", req.body)

        res.status(200).json ({message: "O aluno está cadastrado"});
    }  

    static async editarAluno(req,res) {
    const { nome, idade, profissao, cursoMatriculado } = req.body;
    if( nome !== "" && idade !== "" && profissao !== "" && cursoMatriculado !== "")
    {
        console.log (" O aluno foi editado com os seguintes dados: ", req.body)
        res.status(200).json ({message: "O aluno foi atualizado"});
    }
    else
    {
    res.status(200).json ({message: "  Dados inválidos "});
    }

}

static async deleteAluno(req,res) {
    console.log(req.params.id)
    if( req.params.id !== "")
    {
        res.status(200).json ({message: "  O aluno foi deletado "});
    }
    else
    {
        res.status(200).json ({message: "  O aluno não foi encontrado "});
    }



}

}