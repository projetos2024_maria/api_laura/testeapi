const router = require('express').Router()
const algumaController = require('../controller/teacherController')
const alunoController = require('../controller/alunoController')

router.get('/alguma/', algumaController.getAlgumaCoisa)
router.post('/cadastroaluno/', alunoController.postAluno)
router.put('/editaraluno/', alunoController.editarAluno)
router.delete('/deletaraluno/:id', alunoController.deleteAluno )


module.exports = router